package com.vietis.truonghan.baseprojectandroid_thd.data;

import android.util.LruCache;

public class Cache extends LruCache<String, Object> {

    public Cache() {
        super(1024 * 1024 * 2); // 2MBs
    }

    @SuppressWarnings("unchecked")
    public <T> T fetch(String key) {
        return (T) get(key);
    }
}
