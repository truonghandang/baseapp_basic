package com.vietis.truonghan.baseprojectandroid_thd.domain.connection;

import android.content.Context;

import com.google.gson.Gson;
import com.vietis.truonghan.baseprojectandroid_thd.BuildConfig;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static ApiClient instance;

    private ApiInterface api;
    private static final int TIME_OUT = 10;


    public static ApiClient getInstance(Context context) {
        if (instance == null) {
            instance = new ApiClient(context);
        }
        return instance;
    }

    private ApiClient(Context context) {
        //set cookie request
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        //show log request
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient okHttpclient = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .addInterceptor(new RequestHeaderInterceptor(context))
                .addInterceptor(loggingInterceptor)
                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .build();

        String baseUrl = "BuildConfig.BASE_API";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpclient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        api = retrofit.create(ApiInterface.class);
    }

    public ApiInterface getApi() {
        return api;
    }

    public class RequestHeaderInterceptor implements Interceptor {
        Context context;

        RequestHeaderInterceptor(Context context) {
            this.context = context;
        }

        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            final Request.Builder builder = chain.request().newBuilder();
            // TODO add header request
//            builder.addHeader("name", "value");
            return chain.proceed(builder.build());
        }
    }
}
