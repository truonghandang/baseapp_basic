package com.vietis.truonghan.baseprojectandroid_thd.domain.connection;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("url")
    Call<Object> requestName(@Body Object objParam);
}
