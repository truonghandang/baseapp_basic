package com.vietis.truonghan.baseprojectandroid_thd.domain.callback;

import android.app.Service;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.vietis.truonghan.baseprojectandroid_thd.domain.requestcode.HTTPCode;
import com.vietis.truonghan.baseprojectandroid_thd.domain.requestcode.ResultCode;
import com.vietis.truonghan.baseprojectandroid_thd.model.BaseApiModel;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Wrapper callback for handle error
 * Created by thd on 05/10/2018.
 */
public abstract class Callback<T extends BaseApiModel> implements retrofit2.Callback<T> {

    private static final String TAG = Callback.class.getSimpleName();
    protected Context mContext;

    public Callback(Context context) {
        this.mContext = context;
    }

    public abstract void onResponse(Call<T> call, T response);

    protected abstract boolean isVisibleToUser();

    public void onHandleError(int errorCode) {
        onHandleError(errorCode, true);
    }

    public void onHandleError(int errorCode, boolean isShowMessage) {

        if (mContext instanceof Service) {
            // Background service doesnt need to notify ui error
            return;
        }

        if (isVisibleToUser() && isShowMessage) {
            showErrorMessage(errorCode);
        }
    }

    protected void showErrorMessage(int code) {
        String message = ResultCode.getMessage(mContext, code);
//        CommonUtils.showAlertDialog(mContext, message);
    }

    private boolean isSuccessResponse(int resultCode) {
        return ResultCode.RESPONSE_SUCCESS == resultCode;
    }

    @Override
    public final void onResponse(Call<T> call, Response<T> response) {
        if (!isVisibleToUser()) {
            return;
        }

        if (response.code() != HTTPCode.SUCSESS && response.errorBody() != null) {
            String errorBodyString = "";
            try {
                errorBodyString = response.errorBody().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BaseApiModel errorBody = null;
            try {
                errorBody = new Gson().fromJson(errorBodyString, BaseApiModel.class);
            } catch (JsonParseException e) {
                e.printStackTrace();
            }
            if (errorBody != null) {
                onHandleError(errorBody.getResultCode());
            } else {
                onHandleError(ResultCode.ERROR_CODE);
            }
            return;
        }

        T model = response.body();
        if (model == null) {
            onHandleError(ResultCode.ERROR_CODE);
            return;
        }
        if (isSuccessResponse(model.getResultCode())) {
            onResponse(call, model);
        } else {
            onHandleError(model.getResultCode());
        }
    }

    @Override
    public final void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();
        if (t instanceof SocketTimeoutException) {
            onHandleError(ResultCode.ERROR_TIME_OUT);
        } else if (t instanceof IOException) {
            onHandleError(ResultCode.ERROR_NO_CONNECTION);
        } else {
            onHandleError(ResultCode.ERROR_CODE);
        }
    }
}
