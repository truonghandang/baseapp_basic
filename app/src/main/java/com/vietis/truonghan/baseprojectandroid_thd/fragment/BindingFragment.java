package com.vietis.truonghan.baseprojectandroid_thd.fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BindingFragment<VB extends ViewDataBinding> extends Fragment {
    private VB viewBinding;

    @LayoutRes
    protected abstract int layoutRes();

    protected VB getViewBinding() {
        return viewBinding;
    }

    protected <T extends ViewDataBinding> T createContentView(LayoutInflater inflater,
                                                              ViewGroup parent,
                                                              @LayoutRes int layoutResourceId) {
        return DataBindingUtil.inflate(inflater, layoutResourceId, parent, false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        int layoutRes = layoutRes();
        if (layoutRes > 0) {
            viewBinding = createContentView(inflater, container, layoutRes);
            return viewBinding.getRoot();
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewBinding = null;
    }
}
